alias b := build
alias r := run

@_default:
	just --list

build:
	fasm main.asm asm-greet

run: build
	./asm-greet

@clean:
	rm asm-greet
