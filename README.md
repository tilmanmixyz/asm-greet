# Assembly Greet

## About

This is a simple cli application which asks for your name and greets you.

## Why is it better than your application

- I wrote the application
- It is written in assembly

## Running/Building

### With [Nix](https://nixos.org)

#### Running

```sh
nix run "git+https://codeberg.org/tilmanmixyz/asm-greet"
```

#### Building

```sh
nix build "git+https://codeberg.org/tilmanmixyz/asm-greet"
```

### Without Nix

Dependencies:
- [just](https://just.systems) (Optional)
- [fasm](https://flatassembler.net/)

#### just

##### Running

```sh
git clone https://codeberg.org/tilmanmixyz/asm-greet
cd asm-greet
just run 
```

##### Building

```sh
git clone https://codeberg.org/tilmanmixyz/asm-greet
cd asm-greet
just build
```

#### Without just

```sh
git clone https://codeberg.org/tilmanmixyz/asm-greet
cd asm-greet
fasm main.asm asm-greet
```

If you want to also run the application
```sh
./asm-greet
```

## License

[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)

© Tilman Andre Mix 2023

