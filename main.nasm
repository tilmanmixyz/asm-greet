        global _start

section .text

_start: 
  call _printPrompt
  call _getInput
  call _greeting

  xor rdi, rdi    ; return code 0
  mov rax, 60     ; exit syscall
  syscall	

_printPrompt:
  mov rax, 1      ; write syscall
  mov rdi, 1      ; stdout fd
  mov rsi, prompt
  lea rdx,[promptLen]
  syscall
  ret

_getInput:
  mov rax, 0 ; read syscall
  mov rdi, 0 ; stdin fd
  mov rsi, name
  mov rdx, 100
  syscall
  ret

_greeting:
  ; Print "Moin, "
  mov rax, 1 ; write syscall
  mov rdi, 1 ; stdout fd
  mov rsi, greeting ; value
  lea rdx, [greetingLen]
  syscall

  mov rax, 1 ; write syscall
  mov rdi, 1 ; stdout fd
  mov rsi, name
  mov rdx, 100 ; lenght
  syscall

  ret

section .data

prompt: db "Please input your name: "
promptLen: equ $ - prompt
greeting: db "Moin, "
greetingLen: equ $ - greeting
name: times 100 db 0

