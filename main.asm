format ELF64 executable

SYS_EXIT = 60
SYS_WRITE = 1
SYS_READ = 0

STDOUT_FD = 1
STDIN_FD = 0

EXIT_SUCCESS = 0

NAME_LENGHT = 128

macro write fd, buf, len {
  mov rax, SYS_WRITE
  mov rdi, fd
  mov rsi, buf
  mov rdx, len
  syscall
}

macro read fd, buf, len {
  mov rax, SYS_READ
  mov rdi, fd
  mov rsi, buf
  mov rdx, len
  syscall
}

macro exit code {
  mov rax, SYS_EXIT
  mov rdi, code
  syscall
}

struc string [data] {
  common
  . db data
  .size = $ - .
}

segment readable executable
entry _start

_start:
  write STDOUT_FD, prompt, prompt.size
  read STDIN_FD, name, NAME_LENGHT
  write STDOUT_FD, greeting, greeting.size
  write STDOUT_FD, name, NAME_LENGHT
  exit EXIT_SUCCESS
  
segment readable writeable
prompt string "Please input your name: "
greeting string "Moin, "
name: times NAME_LENGHT db 0
