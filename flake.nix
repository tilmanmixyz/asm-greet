{
  inputs = {
    nix.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };
  outputs = {self, nix}:
    let
      system = "x86_64-linux";
      pkgs = nix.legacyPackages.${system};

      buildInputs = [
        pkgs.fasm
      ];
    in
      {
        devShells.${system}.default = with pkgs; mkShell {
          packages = [
            just
          ];
          inherit buildInputs;
        };

        packages.${system}.default = with pkgs; stdenv.mkDerivation {
          pname = "asm-greet";
          version = "0.1.0";
          src = ./.;
          inherit buildInputs;
          buildPhase = ''
            fasm main.asm asm-greet
          '';
          installPhase = ''
            mkdir -p $out/bin
            cp asm-greet $out/bin/asm-greet
          '';
        };
      };
}
